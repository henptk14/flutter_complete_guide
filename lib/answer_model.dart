class AnswerModel {
  String _answer;
  int _score;

  AnswerModel(this._answer, this._score);

  int get score => _score;

  set score(int value) {
    _score = value;
  }

  String get answer => _answer;

  set answer(String value) {
    _answer = value;
  }
}