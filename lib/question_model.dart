import 'answer_model.dart';

class QuestionModel {
  String _questionText;
  List<AnswerModel> _answers;

  QuestionModel(this._questionText, this._answers);

  List<AnswerModel> get answers => _answers;

  set answers(List<AnswerModel> value) {
    _answers = value;
  }

  String get questionText => _questionText;

  set questionText(String value) {
    _questionText = value;
  }
}