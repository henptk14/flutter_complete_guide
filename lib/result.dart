import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int _resultScore;
  final VoidCallback _resetHandler;

  const Result(this._resultScore, this._resetHandler, {super.key});

  String get resultScore {
    String resultText = "You did it!";
    if (_resultScore <= 8) {
      resultText = "You are awesome and innocent!";
    } else if (_resultScore <= 12) {
      resultText = "Pretty likable";
    } else if (_resultScore <= 16) {
      resultText = "You are ... strange?!";
    } else {
      resultText = "You are bad!";
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultScore,
            style: const TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
          ),
          TextButton(
            onPressed: _resetHandler,
            style: TextButton.styleFrom(foregroundColor: Colors.orange),
            child: const Text(
              "Restart",
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }
}
