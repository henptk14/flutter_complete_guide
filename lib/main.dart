import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/answer_model.dart';
import 'package:flutter_complete_guide/question_model.dart';
import 'package:flutter_complete_guide/quiz.dart';
import 'package:flutter_complete_guide/result.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static final List<QuestionModel> _questions = [
    QuestionModel("What's your favorite color?", [
      AnswerModel("Black", 10),
      AnswerModel("Red", 8),
      AnswerModel("White", 2),
    ]),
    QuestionModel("What's your favorite animal?", [
      AnswerModel("Rabbit", 7),
      AnswerModel("Snake", 5),
      AnswerModel("Elephant", 10),
      AnswerModel("Lion", 8),
    ]),
    QuestionModel("Who's your favorite instructor?", [
      AnswerModel("Max", 6),
      AnswerModel("Stephen Grider", 9),
    ]),
  ];

  int _questionIndex = 0;
  int _totalScore = 0;

  void answerQuestion(int score) {
    if (_questionIndex < _questions.length) {
      _totalScore += score;
      setState(() {
        _questionIndex += 1;
      });
    }
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("My First App"),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(_questions, _questionIndex, answerQuestion)
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
