import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/answer.dart';
import 'package:flutter_complete_guide/question.dart';
import 'package:flutter_complete_guide/question_model.dart';

class Quiz extends StatelessWidget {
  final List<QuestionModel> _questions;
  final int _questionIndex;
  final Function(int) answerQuestion;

  const Quiz(
    this._questions,
    this._questionIndex,
    this.answerQuestion, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(_questions[_questionIndex].questionText),
        ..._questions[_questionIndex].answers.map(
            (e) => Answer(selectHandler: () => answerQuestion(e.score), answerText: e.answer)),
      ],
    );
  }
}
